# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 20:12:51 2018

@author: GABI
"""
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def test_stations_within_radius():
    stations = build_station_list()
    x = stations_within_radius(stations, (52.2053,0.1218),10)
    y = sorted([s[0].name for s in x])
    assert y == ['Bin Brook', 'Cambridge Baits Bite', "Cambridge Byron's Pool",
 'Cambridge Jesus Lock', 'Comberton', 'Dernford', 'Girton',
 'Haslingfield Burnt Mill', 'Lode', 'Oakington', 'Stapleford']
    
test_stations_within_radius()